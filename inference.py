# -*- coding:utf-8 -*-
import os
import sys

import argparse

import torch
import torch.nn as nn
import torch.optim as optim

from generator import Generator
from discriminator.cnn import CNN
from discriminator.attention_lstm import AttentionLSTMModel
from discriminator.attention_indRNN import IndRNNModel
from discriminator.self_attention import SelfAttention
from target_lstm import TargetLSTM
from rollout import Rollout
from logger import Logger
from data_iter import GenDataIter, DisDataIter

from data import Corpus

from eval.test_metrics import compute_bleu
from eval.plot_metrics import plot_metrics
# ================== Parameter Definition =================

parser = argparse.ArgumentParser(description='Training Parameter')

# Model parameters.
parser.add_argument('--checkpoint', type=str, default='',
    help='checkpoint path')

parser.add_argument('--generated_num', type=int, default=20,
    help='number of sequence generated')
parser.add_argument('--sequence_len', type=int, default=20,
    help='number of tokens to generate in each sequence')

parser.add_argument('--seed', type=int, default=None,
    help='initialization seed')
parser.add_argument('--input', type=str, default=None,
    help='input sequence')
parser.add_argument('--evaluate', type=str, default=None,
    help='reference path / test dataset path')
parser.add_argument('--name', type=str, default='None',
    help='save results with name')

parser.add_argument('--output_path', type=str, default='eval/generated.txt',
    help='output text of generated sequences')
parser.add_argument('--metrics_path', type=str, default='eval/log_metric',
    help='path where metric results are saved')
parser.add_argument('--plots_path', type=str, default='eval/plots',
    help='path where plots are saved')

parser.add_argument('--discriminate', action='store_true',
    help='save only sequences considered as real')
parser.add_argument('--classify', action='store_true',
    help='print discriminator evaluation')

opt = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'

real_count = 0

# generate only sequences classified as real by the discriminator
def generate_real_file(corpus, samples, pred, output_path):
    global real_count
    output = open(output_path, 'a')
    eos = corpus.dictionary.get_id('<pad>')
    for i, line in zip(range(0, len(samples)), samples):
        if pred[i].argmax().item() == 0:
            continue
        for token in line:
            if int(token) == eos:
                break
            output.write(corpus.dictionary.get_word(int(token)) + ' ')
        output.write(str(torch.argmax(pred[i]).data.numpy().tolist()) + '\n')
        real_count += 1
        if real_count == opt.generated_num:
            break
    output.close()

# generate text
def generate_file(corpus, samples, output_path):
    i = 0
    output = open(output_path, 'w')
    eos = corpus.dictionary.get_id('<pad>')
    for line in samples:
        for token in line:
            if int(token) == eos: break
            if (int(token) == 0): continue
            output.write(corpus.dictionary.get_word(int(token)) + ' ')
        output.write('\n')
    output.close()

# input to tensor
def _to_tensor(x, corpus, num):
    words = x.split()
    sample = []
    sample.append(corpus.dictionary.get_id('<sos>'))
    for word in words:
        sample.append(corpus.dictionary.get_id(word))
    samples = []
    for i in range(num):
        samples.append(sample)
    return torch.tensor(samples, dtype=torch.long, device=device)

def main():
    global real_count

    # restore checkpoint
    corpus_ckpt = os.path.join(opt.checkpoint, 'dic')
    corpus = torch.load(corpus_ckpt, map_location=device)
    gen_ckpt = os.path.join(opt.checkpoint, 'gen')
    dis_ckpt = os.path.join(opt.checkpoint, 'dis')
    generator, _, _ = torch.load(gen_ckpt, map_location=device)
    discriminator, _, _ = torch.load(dis_ckpt, map_location=device)

    # set seed
    if opt.seed is not None:
        torch.manual_seed(opt.seed)

    # set inference mode
    generator.eval()
    discriminator.eval()

    # setting up
    sequence_len = opt.sequence_len
    generated_num = opt.generated_num
    output_path = opt.output_path
    x = _to_tensor(opt.input, corpus, generated_num) if opt.input is not None else None

    # print only sequences classified as real
    if opt.discriminate:
        while real_count < generated_num:
            samples = generator.sample(generated_num, sequence_len, x)
            _, softmax = discriminator(samples)
            generate_real_file(corpus, samples, softmax, output_path)
    else:
        samples = generator.sample(generated_num, sequence_len, x)
        generate_file(corpus, samples, output_path)

    # evaluate generated sequences with BLEU - POS-BLEU - SELF-BLEU
    if opt.test is not None:
        metric_path, plot_path = opt.metrics_path, opt.plots_path
        if not os.path.exists(metric_path):
            os.mkdir(metric_path)
        if not os.path.exists(plot_path):
            os.mkdir(plot_path)

        with open(metric_path + '/' + opt.name + '.csv', 'a') as f:
            for i in range(2, 6):
                result_bleu, result_posbleu, result_selfbleu = compute_bleu(i, output_path, opt.test, 'bleu pos self')
                f.write(str(i) + ';' + str(result_bleu) + ';' + str(result_posbleu) + ';' + str(result_selfbleu) + ';\n')
                print('BLEU ' , i , ': ', str(result_bleu))
                print('POSBLEU ' , i , ': ', str(result_posbleu))
                print('SELFBLEU ' , i , ': ', str(result_selfbleu))
            for i in range(6, 8):
                result_bleu, result_posbleu, result_selfbleu = compute_bleu(i, output_path, opt.test, 'pos')
                f.write(str(i) + ';' + str(result_bleu) + ';' + str(result_posbleu) + ';' + str(result_selfbleu) + ';\n')
                print('BLEU ' , i , ': ', str(result_bleu))
                print('POSBLEU ' , i , ': ', str(result_posbleu))
                print('SELFBLEU ' , i , ': ', str(result_selfbleu))
        plot_metrics(metric_path, plot_path)

if __name__ == '__main__':
    main()