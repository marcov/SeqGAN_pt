# -*- coding:utf-8 -*-

import os
import random
import math
import copy

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F

from tqdm import tqdm

class Rollout(object):
    """Roll-out policy"""
    def __init__(self, model, update_rate):
        self.ori_model = model
        self.own_model = copy.deepcopy(model)
        self.update_rate = update_rate

    def get_reward(self, x, num, discriminator):
        """
        Args:
            x : (batch_size, seq_len) input data
            num : roll-out number
            discriminator : discrimanator model
        """
        rewards = []
        batch_size = x.size(0)
        seq_len = x.size(1)
        for i in range(num):
            for l in range(1, seq_len):
                data = x[:, 0:l]
                samples = self.own_model.sample(batch_size, seq_len, data)
                logits, _ = discriminator(samples) #, dis_data_iter.get_reference())
                log_softmax = F.log_softmax(logits, dim=-1)
                log_softmax = log_softmax.cpu().data[:, 1].numpy()
                reward = log_softmax
                if i == 0:
                    rewards.append(reward)
                else:
                    rewards[l-1] += reward

            # for the last token
            logits, _ = discriminator(x) #, dis_data_iter.get_reference())
            log_softmax = F.log_softmax(logits, dim=-1)
            log_softmax = log_softmax.cpu().data[:, 1].numpy()
            reward = log_softmax
            if i == 0:
                rewards.append(reward)
            else:
                rewards[seq_len-1] += reward

        rewards = np.transpose(np.array(rewards)) / (1.0 * num) # batch_size * seq_len
        return rewards

    def update_params(self):
        dic = {}
        for name, param in self.ori_model.named_parameters():
            dic[name] = param.data
        for name, param in self.own_model.named_parameters():
            if name.startswith('embedding'):
                param.data = dic[name]
            else:
                param.data = self.update_rate * param.data + (1 - self.update_rate) * dic[name]