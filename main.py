# -*- coding:utf-8 -*-
import os
import random
import sys
import argparse

import torch
import torchtext
from configuration import init_parser
from logger import Logger
# from tensorboardX import SummaryWriter

from trainer import Trainer

from target_lstm import TargetLSTM
from data import Corpus
from data_iter import GenDataIter, DisDataIter

from tqdm import tqdm
from target_lstm import TargetLSTM
import math
import numpy as np

# ================== Parameter Definition =================

parser = init_parser()
opt = parser.parse_args()
print(opt)

# Basic Training Parameters
G_PRE_EPOCHS = opt.g_pre_epochs
D_PRE_STEPS = opt.d_pre_steps
D_PRE_EPOCHS = opt.d_pre_epochs
G_STEPS = opt.g_steps
D_STEPS = opt.d_steps
G_EPOCHS = opt.g_epochs
G_POST_EPOCHS = opt.g_post_epochs
D_EPOCHS = opt.d_epochs
D_BATCH_SIZE = opt.d_batch_size

GAN_EPOCHS = opt.gan_epochs
BATCH_SIZE = opt.batch_size
GENERATED_NUM = opt.generated_num
SEQUENCE_LEN = opt.sequence_len

POSITIVE_FILE = 'save/' + opt.train_data
NEGATIVE_FILE = 'save/' + opt.gene_data
EVAL_FILE = 'save/' + opt.eval_data
TEXT_FILE = 'save/' + opt.text_data

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# generate file
def generate_file(samples, output_file):
    with open(output_file, 'w') as fout:
        for sample in samples:
            string = ' '.join([str(s.item()) for s in sample])
            fout.write('%s\n' % string)

# generate text
def generate_eval(corpus, result=None):
    i = 0
    pad = corpus.dictionary.get_id('<pad>')
    n = open(NEGATIVE_FILE, 'r')
    with open(TEXT_FILE, 'w') as e:
        for line in n.readlines():
            tokens = line.split(' ')
            for token in tokens:
                if int(token) == pad:
                    break
                e.write(corpus.dictionary.get_word(int(token)) + ' ')
            if result is not None and i < len(result):
                e.write('; ' + str(result[i]) + ';\n')
                i += 1
            else:
                e.write('; -1 ;\n')

# generate corpus from file
def generate_corpus(target_data):
    corpus = Corpus(target_data)
    return corpus

def generate_positive(corpus):
    ids = corpus.tokens
    pad = corpus.dictionary.get_id('<pad>')

    with open(POSITIVE_FILE, "w") as f:
        length = 0
        for id in ids:
            value = id.data.cpu().numpy()
            if length == SEQUENCE_LEN and value != pad:
                continue
            if value == pad:
                while length < SEQUENCE_LEN:
                    f.write(str(pad) + ' ')
                    length += 1
                f.write('\n')
                length = 0
            else:
                f.write(str(value) + ' ')
                length += 1

# generate glove embeddings
def generate_embeddings(corpus, dim, name='GloVe'):
    if name == 'GloVe':
        pre_trained = torchtext.vocab.GloVe(name='6B', dim=dim)
    # else if name == 'Word2Vec':
    # else if name == 'FastText':
    dict = corpus.dictionary.itos
    emb = []
    count = []
    for d in dict:
        id = pre_trained.stoi.get(d, -1)
        vector = torch.zeros([dim], dtype=torch.float).normal_(0, 0.1) if id == -1 else pre_trained.vectors[id]
        if id == -1:
            count.append(0)
        else:
            count.append(1)
        emb.append(vector.to(device))
    print(name + ' coverage: %d/%d - %.4f' % (sum(count), len(count), sum(count) / len(count)))
    return torch.stack(emb)

def main():

    os.system('rm -r runs > /dev/null')
    # writer = SummaryWriter()
    log = Logger(str(opt), opt.save)

    # restore trained generator and discriminator
    if opt.resume is not None:
        corpus_path = './checkpoints/' + opt.resume + '/dic'
        corpus = torch.load(corpus_path, map_location=device)
        generate_positive(corpus)
        vocab_size = len(corpus.dictionary)
        pad_token = corpus.dictionary.get_id('<pad>')
        # vocab_size = 5000
        # pad_token = 0

        trainer = Trainer(opt, vocab_size, resume=True, only_gen=opt.only_gen)

        gen_data_iter = GenDataIter(POSITIVE_FILE, BATCH_SIZE, SEQUENCE_LEN, pad_token)
        if opt.only_gen is True:
            print('Pretrain Discriminator ...')
            for i in range(D_PRE_STEPS):
                print('=== Iteration %d ===' % (i))
                samples = trainer.generate(GENERATED_NUM, D_BATCH_SIZE, SEQUENCE_LEN)
                generate_file(samples, NEGATIVE_FILE)
                dis_data_iter = DisDataIter(POSITIVE_FILE, NEGATIVE_FILE, D_BATCH_SIZE, SEQUENCE_LEN, pad_token)
                for epoch in range(D_PRE_EPOCHS):
                    loss = trainer.train_discriminator(dis_data_iter, 0)
                    print('Epoch [%d], loss: %f' % (epoch, loss))
                    log.write_dis(str(i) + ';' + str(epoch) + ';' + str(loss))
                # writer.add_scalar('data/pre_d_loss', loss, (i))

                dis_data_iter = DisDataIter(POSITIVE_FILE, NEGATIVE_FILE, D_BATCH_SIZE, SEQUENCE_LEN, pad_token)
                result_real, result_fake = trainer.discriminate(dis_data_iter)
                print(' - True Positive (1/1): %d / %d' % (sum(result_real), len(result_real)))
                print(' - False Positive (1/0): %d / %d' % (sum(result_fake), len(result_fake)))
                P = sum(result_real) / (sum(result_real) + sum(result_fake))
                R = sum(result_real) / (len(result_real))
                print(' - P: %f - R: %f - F1: %f' % (P, R, 2 * (P * R) / (P + R)))
                generate_eval(corpus, result_fake)

            log.flush()
            # Save pre trained models
            trainer.save(0, corpus)

        # target_lstm = TargetLSTM(vocab_size, opt.g_embed_size, opt.g_hidden_size).to(device)
        # target_lstm.generate_samples(BATCH_SIZE, GENERATED_NUM, SEQUENCE_LEN, POSITIVE_FILE)
        # gen_criterion = torch.nn.CrossEntropyLoss()

    else:
        if not os.path.exists('./save'):
            os.mkdir('./save')
        # Define corpus
        corpus = generate_corpus(opt.target_data)
        generate_positive(corpus)
        vocab_size = len(corpus.dictionary)
        pad_token = corpus.dictionary.get_id('<pad>')
        print('vocab_size: ', vocab_size)

        # Define embeddings
        embedding = None
        if (opt.g_pre_embed != 0):
            embedding = generate_embeddings(corpus, opt.g_pre_embed)

        # Oracle
        # vocab_size = 5000
        # pad_token = 0
        # target_lstm = TargetLSTM(vocab_size, opt.g_embed_size, opt.g_hidden_size).to(device)
        # target_lstm.generate_samples(BATCH_SIZE, GENERATED_NUM, SEQUENCE_LEN, POSITIVE_FILE)
        # gen_criterion = torch.nn.CrossEntropyLoss()

        # Define Networks
        trainer = Trainer(opt, vocab_size, pad_token, embedding)
        trainer.model_stats()

        # Pretrain Generator using MLE
        print('Pretrain Generator with MLE ...')
        gen_data_iter = GenDataIter(POSITIVE_FILE, BATCH_SIZE, SEQUENCE_LEN, pad_token)
        for epoch in range(1, G_PRE_EPOCHS + 1):
            loss = trainer.train_generator(gen_data_iter, 0)
            print('Epoch [%d] Model Loss: %f'% (epoch, loss))
            log.write_gen(str(epoch) + ';' + str(loss))
            # writer.add_scalar('data/pre_g_loss', loss, epoch)

            if (epoch % 20 == 0):
                loss = trainer.eval_generator(gen_data_iter)
                print('Epoch [%d] Perplexity: %f' % (epoch, loss))
                log.write_ppl(str(epoch) + ';' + str(loss))
                # writer.add_scalar('data/perplexity', loss, epoch)
                samples = trainer.generate(GENERATED_NUM, D_BATCH_SIZE, SEQUENCE_LEN)
                generate_file(samples, NEGATIVE_FILE)
                generate_eval(corpus)
                print('=== TEXT.DATA sample (first 10 rows) ===')
                os.system('head -n 10 %s' %(TEXT_FILE))
                print(' ')

            # Oracle
            # if epoch % 5 == 0:
            #     samples = trainer.generate(GENERATED_NUM, BATCH_SIZE, SEQUENCE_LEN)
            #     generate_file(samples, EVAL_FILE)
            #     eval_iter = GenDataIter(EVAL_FILE, BATCH_SIZE, SEQUENCE_LEN)
            #     loss = target_lstm.eval_epoch(eval_iter, gen_criterion)
            #     print(' - Epoch [%d] Oracle Loss: %f' % (epoch, loss))

        # Pretrain Discriminator
        print('Pretrain Discriminator ...')
        for i in range(1, D_PRE_STEPS + 1):
            print('=== Iteration %d ===' % (i))
            samples = trainer.generate(GENERATED_NUM, D_BATCH_SIZE, SEQUENCE_LEN)
            generate_file(samples, NEGATIVE_FILE)
            dis_data_iter = DisDataIter(POSITIVE_FILE, NEGATIVE_FILE, D_BATCH_SIZE, SEQUENCE_LEN, pad_token)
            for epoch in range(D_PRE_EPOCHS):
                loss = trainer.train_discriminator(dis_data_iter, 0)
                print('Epoch [%d], loss: %f' % (epoch, loss))
                log.write_dis(str(i) + ';' + str(epoch) + ';' + str(loss))
            # writer.add_scalar('data/pre_d_loss', loss, (i))

            dis_data_iter = DisDataIter(POSITIVE_FILE, NEGATIVE_FILE, D_BATCH_SIZE, SEQUENCE_LEN, pad_token)
            result_real, result_fake = trainer.discriminate(dis_data_iter)
            print(' - True Positive (1/1): %d / %d' % (sum(result_real), len(result_real)))
            print(' - False Positive (1/0): %d / %d' % (sum(result_fake), len(result_fake)))
            P = sum(result_real) / (sum(result_real) + sum(result_fake))
            R = sum(result_real) / (len(result_real))
            print(' - P: %f - R: %f - F1: %f' % (P, R, 2 * (P * R) / (P + R)))
            # generate_eval(corpus, result_fake)

        log.flush()
        # Save pre trained models
        trainer.save(0, corpus)

    # Adversarial Training
    print('Start Adversarial Training...\n')
    for gan_epoch in range(trainer.get_epoch(), GAN_EPOCHS):
        print('===== Adversarial Epoch %d / %d =====' % (gan_epoch, GAN_EPOCHS))
        ## Train the generator for one step
        for g_step in range(1, G_STEPS + 1):
            loss = trainer.train_gan(BATCH_SIZE, SEQUENCE_LEN)
            print('GAN loss: %f' % (loss))
            log.write_gan(str(gan_epoch) + ';' + str(g_step) + ';' + str(loss.data.cpu().numpy()))
            # writer.add_scalar('data/gan_loss', loss, gan_epoch)

            eval_iter = GenDataIter(POSITIVE_FILE, BATCH_SIZE, SEQUENCE_LEN, pad_token)
            perplexity = trainer.eval_generator(eval_iter)
            print('Perplexity: %f' % (perplexity))
            log.write_ppl(str(G_PRE_EPOCHS + gan_epoch) + ';' + str(perplexity))
            # writer.add_scalar('data/perplexity', perplexity, G_PRE_EPOCHS + gan_epoch)

        trainer.policy_update()

        # Oracle
        # samples = trainer.generate(GENERATED_NUM, BATCH_SIZE, SEQUENCE_LEN)
        # generate_file(samples, EVAL_FILE)
        # eval_iter = GenDataIter(EVAL_FILE, BATCH_SIZE, SEQUENCE_LEN)
        # loss = target_lstm.eval_epoch(eval_iter, gen_criterion)
        # print(' - Epoch [%d] Oracle Loss: %f' % (epoch, loss))

        ## Train the discriminator
        for d_step in range(1, D_STEPS + 1):
            samples = trainer.generate(GENERATED_NUM, BATCH_SIZE, SEQUENCE_LEN)
            generate_file(samples, NEGATIVE_FILE)
            dis_data_iter = DisDataIter(POSITIVE_FILE, NEGATIVE_FILE, D_BATCH_SIZE, SEQUENCE_LEN, pad_token)
            check_loss = trainer.eval_discriminator(dis_data_iter)
            if opt.d_loss_threshold > 0. and check_loss < opt.d_loss_threshold:
                print('Skipped D %f', check_loss)
                break
            for d_epoch in range(1, D_EPOCHS + 1):
                loss = trainer.train_discriminator(dis_data_iter, opt.d_early_stop)
                print('D step[%d] epoch[%d], loss: %f' % (d_step, d_epoch, loss))
                log.write_dis_gan(str(gan_epoch) + ';' + str(d_epoch) + ';' + str(loss))
                iter = (gan_epoch - 1) * D_STEPS * D_EPOCHS + d_step * D_EPOCHS + d_epoch
                # writer.add_scalar('data/d_loss', loss, iter)

        # Print 10 rows of generated text
        dis_data_iter = DisDataIter(POSITIVE_FILE, NEGATIVE_FILE, D_BATCH_SIZE, SEQUENCE_LEN, pad_token)
        result_real, result_fake = trainer.discriminate(dis_data_iter)
        print(' - True Positive (1/1): %d / %d' % (sum(result_real), len(result_real)))
        print(' - False Positive (1/0): %d / %d' % (sum(result_fake), len(result_fake)))
        P = sum(result_real) / (sum(result_real) + sum(result_fake))
        R = sum(result_real) / (len(result_real))
        print(' - P: %f - R: %f - F1: %f' % (P, R, 2 * (P * R) / (P + R)))
        generate_eval(corpus, result_fake)
        print('=== TEXT.DATA sample (first 10 rows) ===')
        os.system('head -n 10 %s' %(TEXT_FILE))
        print(' ')

        if perplexity > opt.ppl_threshold:
            for epoch in range(G_POST_EPOCHS):
                loss = trainer.train_generator(gen_data_iter, opt.g_early_stop)
                print('Epoch [%d] Model Loss: %f'% (epoch, loss))
                log.write_gen(str(epoch) + ';' + str(loss))
                # writer.add_scalar('data/pre_g_loss', loss, epoch)

            if (G_POST_EPOCHS > 0):
                loss = trainer.eval_generator(gen_data_iter)
                print('Epoch [%d] Perplexity: %f' % (gan_epoch, loss))
                log.write_ppl(str(gan_epoch) + ';' + str(loss))
                # writer.add_scalar('data/perplexity_gan', loss, gan_epoch)

        # Save trained models
        if gan_epoch % opt.checkpoint_interval == 0:
            trainer.save(gan_epoch, corpus)
        log.flush()
    log.close()

if __name__ == '__main__':
    main()
