# -*- coding:utf-8 -*-
import os
import zipfile
import math
import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim

from generator import Generator
from discriminator.cnn import CNN
from discriminator.self_attention import SelfAttention
from discriminator.attention_lstm import AttentionLSTMModel
from discriminator.attention_indRNN import IndRNNModel
# from discriminator.ranker import Ranker

from data_iter import GenDataIter, DisDataIter
from rollout import Rollout
from rewarded_loss import Rewarded_NLLLoss

from pathlib import Path
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive
from oauth2client.client import GoogleCredentials

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# class RankNLLLoss(nn.Module):
# 		def __init__(self):
# 			super(RankNLLLoss, self).__init__()

# 		def forward(self, log_softmax, target):
# 			target = target.type(torch.float)
# 			target_n = torch.add(target, -1)
# 			target_n = torch.mul(target_n, -1)
# 			losses_minus = log_softmax * target_n
# 			losses_posit = log_softmax * target
# 			sum_minus = torch.tensor([torch.sum(target_n)])
# 			sum_posit = torch.tensor([torch.sum(target)])
# 			lower_bound = torch.tensor([1e-5])

# 			loss = - (torch.sum(losses_minus) / torch.max(torch.cat((sum_minus, lower_bound), dim=0)) + \
# 					torch.sum(losses_posit) / torch.max(torch.cat((sum_posit, lower_bound), dim=0)) ) / 4
# 			return loss

class Trainer():

	def __init__(self, opt, vocab_size, pad_token=0, pre_embed=None, resume=False, only_gen=False):
		# Trainer Parameters
		self.save_path = opt.save
		self.resume_path = opt.resume
		self.drive_uri = opt.gdrive
		if self.drive_uri is not None:
			self.gauth, self.drive = self._init_drive()

		if resume is False:
			self._init_generator(opt, vocab_size, pre_embed)
			self._init_discriminator(opt, vocab_size, pad_token, pre_embed)
		else:
			if only_gen is False:
				self._restore_trainer()
			else:
				self._restore_trainer(only_gen=True)
				self._init_discriminator(opt, vocab_size, pad_token, pre_embed)

				# init adversarial
				self.rollout = Rollout(self.generator, 0.8)
				self.gen_gan_criterion = Rewarded_NLLLoss()
				self.gen_gan_optimizer = optim.Adam(self.generator.parameters(), lr=opt.gan_lr)
				self.epoch = 1

	def _init_generator(self, opt, vocab_size, pre_embed):
		# Generator Parameters
		g_embed_size = opt.g_embed_size
		g_hidden_size = opt.g_hidden_size
		g_num_layers = opt.g_num_layers
		g_embed_dropout = opt.g_embed_dropout
		g_layer_dropout = opt.g_layer_dropout
		g_out_dropout = opt.g_out_dropout
		g_temperature = opt.g_temperature

		# Optimizer Parameters
		g_opt_wdecay = opt.g_opt_wdecay
		g_opt_lr = opt.g_opt_lr
		gan_lr = opt.gan_lr

		# generator
		self.generator = Generator(vocab_size, g_embed_size, g_hidden_size, g_num_layers,
			g_embed_dropout, g_layer_dropout, g_out_dropout, pre_embed).to(device)
		self.gen_criterion = nn.CrossEntropyLoss().to(device)
		self.gen_test_criterion = nn.CrossEntropyLoss().to(device)
		self.gen_optimizer = optim.Adam(self.generator.parameters(), lr=g_opt_lr, weight_decay=g_opt_wdecay)

		# init adversarial
		self.rollout = Rollout(self.generator, 0.8)
		self.gen_gan_criterion = Rewarded_NLLLoss()
		self.gen_gan_optimizer = optim.Adam(self.generator.parameters(), lr=opt.gan_lr)
		self.epoch = 1

	def _init_discriminator(self, opt, vocab_size, pad_token, pre_embed):
		# Discriminator Parameters
		d_embed_size = opt.d_embed_size
		d_filter_sizes = list(map(int, opt.d_filter_sizes.split(',')))
		d_num_filters = list(map(int, opt.d_num_filters.split(',')))
		d_dropout = opt.d_dropout
		d_num_class = opt.d_num_class

		d_opt_lr = opt.d_opt_lr

		# discriminator
		if opt.d_type == 'cnn':
			self.discriminator = CNN(d_num_class, vocab_size, d_embed_size, d_filter_sizes, d_num_filters, d_dropout, pre_embed)
		elif opt.d_type == 'self':
			self.discriminator = SelfAttention(d_num_class, d_embed_size, vocab_size, d_embed_size, d_dropout, pre_embed)
		elif opt.d_type == 'lstm':
			self.discriminator = AttentionLSTMModel(d_num_class, d_embed_size, vocab_size, d_embed_size, d_dropout, pre_embed)
		elif opt.d_type == 'indrnn':
			self.discriminator = IndRNNModel(d_num_class, d_embed_size, vocab_size, d_embed_size, d_dropout, pre_embed)
		# self.discriminator = Ranker(d_num_class, vocab_size, d_embed_size, d_filter_sizes, d_num_filters, d_dropout).to(device)
		# self.dis_criterion = RankNLLLoss().to(device)
		self.discriminator = self.discriminator.to(device)
		self.dis_criterion = nn.CrossEntropyLoss().to(device)
		self.dis_optimizer = optim.Adam(self.discriminator.parameters(), lr=d_opt_lr)

	def _restore_trainer(self, only_gen=False):
		if self.resume_path is not None:
			resume_path = './checkpoints/' + self.resume_path
			if not Path(resume_path).exists():
				print('checkpoint path does not exist')
				return 0
			gen_ckpt = os.path.join(resume_path, 'gen')
			dis_ckpt = os.path.join(resume_path, 'dis')
			gan_ckpt = os.path.join(resume_path, 'gan')
			dic_ckpt = os.path.join(resume_path, 'dic')
			self.corpus = torch.load(dic_ckpt, map_location=device)
			self.generator, self.gen_criterion, self.gen_optimizer = torch.load(gen_ckpt, map_location=device)

			self.vocab_size = len(self.corpus.dictionary)
			self.gen_test_criterion = nn.CrossEntropyLoss().to(device)

			if only_gen is False:
				self.discriminator, self.dis_criterion, self.dis_optimizer = torch.load(dis_ckpt, map_location=device)
				self.rollout, self.gen_gan_criterion, self.gen_gan_optimizer, self.epoch = torch.load(gan_ckpt, map_location=device)

	def train_generator(self, data_iterator, early_stop):
		total_loss = []
		data_iterator.reset()
		for data, target, length in data_iterator:
			data = data.to(device)
			target = target.to(device)
			target = target.view(-1)
			logits = self.generator(data, length)
			loss = self.gen_criterion(logits, target)
			total_loss.append(loss.item())
			self.gen_optimizer.zero_grad()
			loss.backward()
			self.gen_optimizer.step()

			early_stop -= 1
			if early_stop == 0:
				break
		return np.mean(total_loss)

	def train_discriminator(self, data_iterator, early_stop):
		total_loss = []
		data_iterator.reset()
		for data, target in data_iterator:
			data = data.to(device)
			target = target.to(device)
			target = target.view(-1)
			logits, _ = self.discriminator(data)
			loss = self.dis_criterion(logits, target)
			total_loss.append(loss.item())
			self.dis_optimizer.zero_grad()
			loss.backward()
			self.dis_optimizer.step()

			early_stop -= 1
			if early_stop == 0:
				break
		return np.mean(total_loss)

	def train_gan(self, batch_size, sequence_len):
		samples = self.generator.sample(batch_size, sequence_len)
		# construct the input to the genrator, add zeros before samples and delete the last column
		zeros = torch.zeros((batch_size, 1), dtype=torch.long, device=device)
		inputs = torch.cat([zeros, samples.data], dim=1)[:, :-1].contiguous()
		targets = samples.data.contiguous().view((-1,))
		# calculate the reward
		rewards = self.rollout.get_reward(samples, 16, self.discriminator)
		rewards = torch.tensor(rewards, device=device)
		rewards = torch.exp(rewards).contiguous().view((-1,))
		logits = self.generator(inputs)
		loss = self.gen_gan_criterion(logits, targets, rewards)
		self.gen_gan_optimizer.zero_grad()
		loss.backward()
		self.gen_gan_optimizer.step()
		return loss

	def policy_update(self):
		self.rollout.update_params()

	def get_epoch(self):
		return self.epoch

	def generate(self, generated_num, batch_size, sequence_len):
		samples = []
		for _ in range(int(generated_num / batch_size)):
			sample = self.generator.sample(batch_size, sequence_len)
			samples.extend(sample)
		return samples

	def discriminate(self, data_iterator):
		result_real = []
		result_fake = []
		for data, target in data_iterator:
			data = data.to(device)
			target = target.to(device)
			target = target.contiguous().view(-1)
			logits, _ = self.discriminator(data)

			for i in range(0, len(logits)):
				label = logits[i].argmax().item()
				if target[i].data == 1:
					result_real.append(label)
				if target[i].data == 0:
					result_fake.append(label)

		return result_real, result_fake

	def eval_generator(self, data_iterator):
		total_loss = []
		data_iterator.reset()
		for data, target, length in data_iterator:
			with torch.no_grad():
				data = data.to(device)
				target = target.to(device)
				target = target.contiguous().view(-1)
				softmax = self.generator(data)
				loss = self.gen_test_criterion(softmax, target)
				total_loss.append(loss.item())
		return math.exp(np.mean(total_loss))

	def eval_discriminator(self, data_iterator):
		total_loss = []
		data_iterator.reset()
		for data, target in data_iterator:
			with torch.no_grad():
				data = data.to(device)
				target = target.to(device)
				target = target.view(-1)
				logits, _ = self.discriminator(data)
				loss = self.dis_criterion(logits, target)
				total_loss.append(loss.item())
		return np.mean(total_loss)

	def save(self, epoch, corpus):
		if self.save_path is not None:
			if not os.path.exists('./checkpoints'):
				os.mkdir('./checkpoints')
			checkpoint_name = self.save_path + '_' + str(epoch)
			checkpoint_path = './checkpoints/' + checkpoint_name
			if not os.path.exists(checkpoint_path):
				os.mkdir(checkpoint_path)
			gen_ckpt = os.path.join(checkpoint_path, 'gen')
			dis_ckpt = os.path.join(checkpoint_path, 'dis')
			gan_ckpt = os.path.join(checkpoint_path, 'gan')
			dic_ckpt = os.path.join(checkpoint_path, 'dic')
			torch.save([self.generator, self.gen_criterion, self.gen_optimizer], gen_ckpt)
			torch.save([self.discriminator, self.dis_criterion, self.dis_optimizer], dis_ckpt)
			torch.save([self.rollout, self.gen_gan_criterion, self.gen_gan_optimizer, epoch], gan_ckpt)
			torch.save(corpus, dic_ckpt)
			# writer.add_embedding(generator.get_emb(), metadata=corpus.dictionary.stoi.keys(), tag='Embeddings', global_step=0)
			if self.drive_uri is not None:
				if not os.path.exists('./zips'):
					os.mkdir('./zips')
				zip_path = 'zips/' + checkpoint_name + '.zip'
				self._zip_checkpoint(zip_path, checkpoint_path, './logs/log_' + self.save_path)
				self._upload_to_gdrive(zip_path)

	def _init_drive(self):
		from google.colab import auth
		auth.authenticate_user()
		gauth = GoogleAuth()
		gauth.credentials = GoogleCredentials.get_application_default()
		drive = GoogleDrive(gauth)
		return gauth, drive

	def _upload_to_gdrive(self, path):
		if self.gauth.access_token_expired:
			self.gauth.Refresh()
			drive = GoogleDrive(self.gauth)
		uploaded = self.drive.CreateFile({"parents": [{"kind": "drive#fileLink", "id": self.drive_uri}]})
		uploaded.SetContentFile('%s' % (path))
		uploaded.Upload()
		print('Uploaded file with ID {}'.format(uploaded.get('id')))

	def _zip_checkpoint(self, zip_path, checkpoint_path, log_path):
		zip_file = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED)
		# save checkpoint files
		for root, _, files in os.walk(checkpoint_path):
			for file in files:
				zip_file.write(os.path.join(root, file), file)
		# save logs
		for root, dirs, files in os.walk(log_path):
			zip_file.write(root)
			for file in files:
				arcname = 'logs/' + str(file)
				zip_file.write(os.path.join(root, file), arcname)
		# save output
		for root, dirs, files in os.walk('./save'):
			zip_file.write(root)
			for file in files:
				zip_file.write(os.path.join(root, file))
		# save tensorboardx files
		for root, dirs, files in os.walk('./runs'):
			zip_file.write(root)
			for file in files:
				zip_file.write(os.path.join(root, file))
		zip_file.close()

	def model_stats(self):
		gen_params = sum(p.numel() for p in self.generator.parameters())
		dis_params = sum(p.numel() for p in self.discriminator.parameters())
		total_params = gen_params + dis_params
		stats = 'Parameters stats: \n' + \
				' + Generator: ' + str(gen_params) + '\n' + \
				' + Discriminator: ' + str(dis_params) + '\n' + \
				' = Total: ' + str(total_params)
		print(stats)