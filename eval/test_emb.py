# -*- coding:utf-8 -*-

import os
import random
import math
import sys

import argparse

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchtext.vocab as vocab

sys.path.append('.')
from generator import Generator

from data import Corpus

from tqdm import tqdm

# ================== Parameter Definition =================

parser = argparse.ArgumentParser(description='Training Parameter')

# Model parameters.
parser.add_argument('--checkpoint1', type=str, default=None)
parser.add_argument('--checkpoint2', type=str, default=None)

opt = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'


def main():
    corpus_ckpt = os.path.join(opt.checkpoint1, 'corpus')
    corpus = torch.load(corpus_ckpt, map_location=device)

    gen1_ckpt = os.path.join(opt.checkpoint1, 'gen')    
    generator1, _, _ = torch.load(gen1_ckpt, map_location=device)
    generator1.eval()

    gen2_ckpt = os.path.join(opt.checkpoint2, 'gen')    
    generator2, _, _ = torch.load(gen2_ckpt, map_location=device)
    generator2.eval()

    emb1 = generator1.get_emb()  
    emb2 = generator2.get_emb()  
    
    word_list1 = [] 
    word_list2 = []
    for i in range(emb1.size()[0]):
        word_list1.append([])
        word_list2.append([])

    print('Computing distances...')
    for i in tqdm(range(emb1.size()[0])):
        word = corpus.dictionary.itos[i]

        e11 = emb1[i]
        e21 = emb2[i]
        for j in range(i+1, emb1.size()[0]):
            e12 = emb1[j]
            e22 = emb2[j]

            distance1 = abs(F.cosine_similarity(e11, e12, 0, 1e-8).data.item())
            distance2 = abs(F.cosine_similarity(e21, e22, 0, 1e-8).data.item())
            word_list1[i].append(distance1)            
            word_list2[i].append(distance2)

    print('Computing stats...')
    with open('result_emb.csv', 'w') as f:
        f.write('word;mean_distance_glove;mean_distance_no_glove;mean_difference;\n')        
        mean1 = np.mean(word_list1[0])
        mean2 = np.mean(word_list2[0])
        meand = np.mean(abs(word_list1[0] - word_list2[0]))
        s = '%s;%f;%f;%f;\n' % (corpus.dictionary.itos[0], mean1, mean2, meand)
        f.write(s)
        for i in tqdm(range(1, emb1.size()[0])):
            wl1 = np.hstack((word_list1[i], [word_list1[r][c] for r, c in zip(range(0, i), range(i-1, -1, -1))]))
            wl2 = np.hstack((word_list2[i], [word_list2[r][c] for r, c in zip(range(0, i), range(i-1, -1, -1))]))
            mean1 = np.mean(wl1)
            mean2 = np.mean(wl2)
            meand = np.mean(abs(wl1 - wl2))
            s = '%s;%f;%f;%f\n' % (corpus.dictionary.itos[i], mean1, mean2, meand)
            f.write(s)

if __name__ == '__main__':
    main()