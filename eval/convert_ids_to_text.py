import utils.text_process as tp

data_loc = 'data/image_coco.txt'
generator_file = 'drive/save_seqgan/generator.txt'
test_file = 'drive/save_seqgan/test_file.txt'
sequence_length, vocab_size = tp.text_precess(data_loc)

tokens = tp.get_tokenlized(data_loc)
word_set = tp.get_word_list(tokens)
[word_index_dict, index_word_dict] = tp.get_dict(word_set)

outfile = 'drive/sa'
with open(generator_file, 'r') as file:
    codes = tp.get_tokenlized(generator_file)
with open(test_file, 'w') as outfile:
    outfile.write(tp.code_to_text(codes=codes, dictionary=dict))
