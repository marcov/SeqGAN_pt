import os
import json
from multiprocessing import Pool

import nltk
from nltk.translate.bleu_score import SmoothingFunction

from pythonrouge.pythonrouge import Pythonrouge
from utils.metrics.Metrics import Metrics

class Rouge_metric(Metrics):
    def __init__(self, test_text='', real_text=''):
        super().__init__()
        self.name = 'Rouge'
        self.test_data = test_text
        self.real_data = real_text        
        self.sample_size = 500
        self.reference = None
        self.is_first = True

    def get_name(self):
        return self.name

    def get_reference(self):
        if self.reference is None:
            reference = list()
            with open(self.real_data) as real_data:
                for text in real_data:
                    # text = nltk.word_tokenize(text)
                    reference.append(text)
            self.reference = reference
            return reference
        else:
            return self.reference

    def get_rouge(self):        
        rouge1 = list()
        rouge2 = list()
        rougel = list()
        reference = [[self.get_reference()]]
        with open(self.test_data) as test_data:
            for hypothesis in test_data:                
                hypothesis = [[hypothesis]]                
                rouge = Pythonrouge(summary_file_exist=False,
                    summary=hypothesis, reference=reference,
                    n_gram=2, ROUGE_SU4=False, ROUGE_L=True,
                    recall_only=True, stemming=True, stopwords=True,
                    word_level=True, length_limit=True, length=50,
                    use_cf=False, cf=95, scoring_formula='average',
                    resampling=True, samples=1000, favor=True, p=0.5)
                parsed = json.loads(str(rouge.calc_score()).replace('\'', '"'))
                rouge1.append(float(parsed['ROUGE-1']))
                rouge2.append(float(parsed['ROUGE-2']))
                rougel.append(float(parsed['ROUGE-L']))                
        return sum(rouge1) / len(rouge1), sum(rouge2) / len(rouge2), sum(rougel) / len(rougel)

    def calc_rouge(self, reference, hypothesis):
        hypothesis = [[hypothesis]]                
        rouge = Pythonrouge(summary_file_exist=False,
            summary=hypothesis, reference=reference,
            n_gram=2, ROUGE_SU4=False, ROUGE_L=True,
            recall_only=True, stemming=True, stopwords=True,
            word_level=True, length_limit=True, length=50,
            use_cf=False, cf=95, scoring_formula='average',
            resampling=True, samples=1000, favor=True, p=0.5)
        parsed = json.loads(str(rouge.calc_score()).replace('\'', '"'))
        return [float(parsed['ROUGE-1']), float(parsed['ROUGE-2']), float(parsed['ROUGE-L'])]

    def get_rouge_parallel(self, reference=None):        
        if reference is None:
            reference = [[self.get_reference()]]        
        pool = Pool(os.cpu_count())
        result = list()
        with open(self.test_data) as test_data:
            for hypothesis in test_data:
                hypothesis = [[hypothesis]]                
                result.append(pool.apply_async(self.calc_rouge, args=(reference, hypothesis)))
        rouge1 = 0.0
        rouge2 = 0.0
        rougel = 0.0
        cnt = 0             
        for i in result:            
            rouge1 += i.get()[0]
            rouge2 += i.get()[1]
            rougel += i.get()[2]
            cnt += 1
        pool.close()
        pool.join()
        return rouge1 / cnt, rouge2 / cnt, rougel / cnt
