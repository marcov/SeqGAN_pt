import os
from multiprocessing import Pool

import nltk
from nltk.translate.bleu_score import SmoothingFunction

from utils.metrics.Metrics import Metrics

from tqdm import tqdm

class POSBleu(Metrics):
    def __init__(self, test_text='', real_text='', gram=3):
        super().__init__()
        self.name = 'POSBleu'
        self.test_data = test_text
        self.real_data = real_text
        self.gram = gram
        self.sample_size = 500
        self.reference = None
        self.is_first = True

    def get_name(self):
        return self.name

    def get_reference(self):
        if self.reference is None:
            reference = list()
            with open(self.real_data) as real_data:
                for text in real_data:
                    text = self.get_tokenized_pos_string(text)
                    reference.append(text)
            self.reference = reference
            return reference
        else:
            return self.reference

    def get_tokenized_pos_string(self, s):
        s_tokenized = nltk.word_tokenize(s)
        s_pos = []
        for tag in nltk.pos_tag(s_tokenized) :
            s_pos.append (tag[1])
        return s_pos

    def get_bleu(self):
        ngram = self.gram
        bleu = list()
        reference = self.get_reference()
        weight = tuple((1. / ngram for _ in range(ngram)))
        with open(self.test_data) as test_data:
            for hypothesis in test_data:
                hypothesis = self.get_tokenized_pos_string(hypothesis)
                bleu.append(nltk.translate.bleu_score.sentence_bleu(reference, hypothesis, weight,
                                                                    smoothing_function=SmoothingFunction().method1))
        return sum(bleu) / len(bleu)

    def calc_bleu(self, reference, hypothesis, weight):
        return nltk.translate.bleu_score.sentence_bleu(reference, hypothesis, weight,
                                                       smoothing_function=SmoothingFunction().method1)

    def get_bleu_parallel(self, reference=None):
        ngram = self.gram
        if reference is None:
            reference = self.get_reference()
        weight = tuple((1. / ngram for _ in range(ngram)))
        pool = Pool(os.cpu_count())
        result = list()
        with open(self.test_data) as test_data:
            for hypothesis in test_data:
                hypothesis = self.get_tokenized_pos_string(hypothesis)
                result.append(pool.apply_async(self.calc_bleu, args=(reference, hypothesis, weight)))
        score = 0.0
        cnt = 0
        for i in tqdm(result):
            score += i.get()
            cnt += 1
        pool.close()
        pool.join()
        return score / cnt



