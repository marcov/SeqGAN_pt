#!/bin/sh
if [ -n "$1" ] && [ -n "$2" ]
then
    echo "test $1"
    echo "real $2"
fi

rm result_bleu.csv > /dev/null

for i in $(seq 2 5);
    do 
        echo " === Computing BLEU $i and POSBLEU $i ==="
        if [ -n "$1" ] && [ -n "$2" ]
        then
            python3 test_metrics.py --gram $i --test $1 --real $2
        else
            python3 test_metrics.py --gram $i
        fi
    done

for i in $(seq 6 7);
    do 
        echo " === Computing POSBLEU $i ==="
        if [ -n "$1" ] && [ -n "$2" ]
        then
            python3 test_metrics.py --gram $i --pos_only True --test $1 --real $2
        else
            python3 test_metrics.py --gram $i --pos_only True
        fi
    done
