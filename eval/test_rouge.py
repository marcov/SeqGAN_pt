from time import time
from utils.metrics.Rouge_metric import Rouge_metric


start = time()
rouge = Rouge_metric(test_text='eval.txt', real_text='../image_coco/image_coco.txt')
r1, r2, rl = rouge.get_rouge_parallel()
end = time()
print('R1: ', r1, ' R2: ', r2, ' Rl: ', rl, ' time: ', str(end - start))