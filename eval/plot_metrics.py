import os
import csv
import math
import argparse

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as mpath
from matplotlib.ticker import MaxNLocator

def plot_gan_loss(loss_path, plots_path):
    logs = []
    files = []
    for filename in os.listdir(loss_path):
        files.append(str(filename).replace('.txt',''))
        path = os.path.join(loss_path, filename)
        with open(path, 'r') as f:
            csv_reader = csv.reader(f, delimiter=';')
            loss = []
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    loss.append(float(row[2]))
                    line_count += 1
            logs.append(loss)

    ylim = 0
    for log, name in zip(logs, files):
        plt.plot(range(len(log)), log, label=name)
        ylim = max(ylim, max(log))
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(loc='best')
    plt.title('gan loss')
    plt.savefig(plots_path + '/gan_loss.png')
    plt.show()

def plot_loss_and_perplexity(loss_path, plots_path):
    logs = []
    files = []
    for filename in os.listdir(loss_path):
        print(filename)
        files.append(str(filename).replace('.txt',''))
        path = os.path.join(loss_path, filename)
        with open(path, 'r') as f:
            csv_reader = csv.reader(f, delimiter=';')
            loss = []
            line_count = 0
            for row in csv_reader:
                if line_count == 0:
                    line_count += 1
                else:
                    loss.append(float(row[1]))
                    line_count += 1
            logs.append(loss)

    ylim = 0
    plt.figure(1)
    for log, name in zip(logs, files):
        plt.plot(range(len(log)), log, label=name)
        ylim = max(ylim, max(log))
    plt.ylabel('perplexity')
    plt.xlabel('epoch')
    plt.legend(loc='best')
    plt.title('perplexity')
    plt.savefig(plots_path + '/perplexity.png')

    plt.figure(2)
    for log, name in zip(logs, files):
        plt.plot(range(len(log)), [math.log(l) for l in log], label=name)
        ylim = max(ylim, max(log))
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(loc='best')
    plt.title('gen_loss')
    plt.savefig(plots_path + '/gen_loss.png')

    plt.show()

def plot_metrics(metric_path, plots_path, name=None):

    if not os.path.exists(plots_path):
        os.mkdir(plots_path)

    files = []
    bleu = []
    posbleu = []
    selfbleu = []
    start_bleu, start_posbleu, start_selfbleu = 10, 10, 10
    for filename in os.listdir(metric_path):
        files.append(filename.replace('.csv', ''))
        files.sort()
    for filename in files:
        path = os.path.join(metric_path, filename + '.csv')
        with open(path, 'r') as f:
            for line in f:
                field = line.split(';')
                if '-1' not in field[1]:
                    start_bleu = min(start_bleu, int(field[0]))
                    bleu.append(float(field[1]))
                if '-1' not in field[2]:
                    start_posbleu = min(start_posbleu, int(field[0]))
                    posbleu.append(float(field[2]))
                if '-1' not in field[3]:
                    start_selfbleu = min(start_selfbleu, int(field[0]))
                    selfbleu.append(float(field[3]))
    bleu = np.reshape(bleu, (len(files), -1))
    posbleu = np.reshape(posbleu, (len(files), -1))
    selfbleu = np.reshape(selfbleu, (len(files), -1))

    w = 0.75 / len(files)
    # plot bleu
    start = np.array([2, 3, 4, 5])
    end = np.array([2, 3, 4, 5])
    width = np.full(4, w)
    min_y, max_y = 1., 0.
    plt.figure(1)
    for row, file, i in zip(bleu, files, range(len(files))):
        end = np.add(end, width)
        plt.bar(end, row, width, linewidth=1, edgecolor='black', label=file)
        min_y, max_y = min(row), max(row)

    tick = math.ceil(len(files)/2) * w if len(files) % 2 != 0 else (len(files)/2 * w) + w/2
    plt.ylim((max(min(min_y - 0.2, 0.), 0.), max(max_y + 0.1, 1.)))
    plt.title(name + ' - BLEU')
    plt.ylabel('Score')
    plt.xlabel('N-Gram')
    plt.xticks(start + tick, start)
    plt.legend(loc='best')
    plt.savefig(plots_path + '/' + name + '_bleu.png')

    # plot posbleu
    start = np.array([2, 3, 4, 5, 6, 7])
    end = np.array([2, 3, 4, 5, 6, 7])
    width = np.full(6, w)
    min_y, max_y = 1., 0.
    plt.figure(2)
    colors = ['blue', 'orange', 'green', 'red', 'violet']
    for row, file, i in zip(posbleu, files, range(len(files))):
        end = np.add(end, width)
        plt.bar(end, row, width, linewidth=1, edgecolor='black', label=file)
        min_y, max_y = min(row), max(row)

    tick = math.ceil(len(files)/2) * w if len(files) % 2 != 0 else (len(files)/2 * w) + w/2
    plt.ylim(min(min_y - 0.2, 0.), max(max_y + 0.5, 1.))
    plt.title(name + ' - POS-BLEU')
    plt.ylabel('Score')
    plt.xlabel('N-Gram')
    plt.xticks(start + tick, start)
    plt.legend(loc='best')
    plt.savefig(plots_path + '/' + name + '_posbleu.png')

    # plot self-bleu
    start = np.array([2, 3, 4, 5])
    end = np.array([2, 3, 4, 5])
    width = np.full(4, w)
    min_y, max_y = 1., 0.
    plt.figure(3)
    colors = ['blue', 'orange', 'green', 'red', 'violet']
    for row, file, i in zip(selfbleu, files, range(len(files))):
        end = np.add(end, width)
        plt.bar(end, row, width, linewidth=1, edgecolor='black', label=file)
        min_y, max_y = min(row), max(row)

    tick = math.ceil(len(files)/2) * w if len(files) % 2 != 0 else (len(files)/2 * w) + w/2
    plt.ylim((max(min(min_y - 0.2, 0.), 0.), max(max_y + 0.1, 1.)))
    plt.title(name + ' - SELF-BLEU')
    plt.ylabel('Score')
    plt.xlabel('N-Gram')
    plt.xticks(start + tick, start)
    plt.legend(loc='best')
    plt.savefig(plots_path + '/' + name + '_selfbleu.png')

    plt.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parameters')
    parser.add_argument('--perplexity_path', type=str, default=None)
    parser.add_argument('--metric_path', type=str, default=None)
    parser.add_argument('--gan_loss_path', type=str, default=None)
    parser.add_argument('--save_path', type=str, default='plots')
    parser.add_argument('--plot_title', type=str, default='Title')
    opt = parser.parse_args()

    # save plots in path
    save_path = opt.save_path

    if not os.path.exists(opt.save_path):
        os.mkdir(opt.save_path)

    # plot perplexity and loss
    if opt.perplexity_path is not None:
        plot_loss_and_perplexity(opt.perplexity_path, opt.plots_path)

    # plot BLEU - POS-BLEU - SELF-BLEU
    if opt.metric_path is not None:
        plot_metrics(opt.metric_path, opt.save_path, opt.plot_title)

    # plot GAN LOSS
    if opt.gan_loss_path is not None:
        plot_metrics(opt.gan_loss_path, opt.save_path)