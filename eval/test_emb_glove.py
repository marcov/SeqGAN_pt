# -*- coding:utf-8 -*-

import os
import random
import math
import sys

import argparse

import numpy as np

import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
import torchtext.vocab as vocab

sys.path.append('.')
from generator import Generator

from data import Corpus
# ================== Parameter Definition =================

parser = argparse.ArgumentParser(description='Training Parameter')

# Model parameters.
parser.add_argument('--checkpoint', type=str, default=None)
parser.add_argument('--dim', type=str, default=50)

opt = parser.parse_args()

device = 'cuda' if torch.cuda.is_available() else 'cpu'


def main():
    corpus_ckpt = os.path.join(opt.checkpoint, 'dic')
    corpus = torch.load(corpus_ckpt, map_location=device)

    gen_ckpt = os.path.join(opt.checkpoint, 'gen')
    generator, _, _ = torch.load(gen_ckpt, map_location=device)
    generator.eval()

    emb = generator.get_embedding_weight()
    glove = vocab.GloVe(name='6B', dim=opt.dim)

    distances = []
    for i in range(emb.size()[0]):
        word = corpus.dictionary.itos[i]
        id_g = glove.stoi.get(word, -1)
        emb_g = glove.vectors[id_g]

        distance = abs(F.cosine_similarity(emb[i], emb_g, 0, 1e-8).data.item())
        distances.append(distance)

    print('AVG: ', np.mean(distances))

if __name__ == '__main__':
    main()