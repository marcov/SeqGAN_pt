import os
import argparse
import sys

from time import time
sys.path.append('eval')
from utils.metrics.Bleu import Bleu
from utils.metrics.POSBleu import POSBleu
from utils.metrics.SelfBleu import SelfBleu

parser = argparse.ArgumentParser()
parser.add_argument('--gram', type=int, default=2)
parser.add_argument('--test', type=str, default='generated.txt', help='hypothesis')
parser.add_argument('--real', type=str, default='../image_coco/test_coco.txt', help='reference')
parser.add_argument('--output', type=str, default='result.csv')
parser.add_argument('--metric', type=str, default='bleu pos self', help='[bleu pos self]')

def compute_bleu(gram, test_text, real_text, metric_list):

    if 'bleu' in metric_list:
        bleu = Bleu(test_text=test_text, real_text=real_text, gram=gram)
        result_bleu = bleu.get_bleu_parallel()
    else:
        result_bleu = -1

    if 'pos' in metric_list:
        posbleu = POSBleu(test_text=test_text, real_text=real_text, gram=gram)
        result_posbleu = posbleu.get_bleu_parallel()
    else:
        result_posbleu = -1

    if 'self' in metric_list:
        selfbleu = SelfBleu(test_text=test_text, gram=gram)
        result_selfbleu = selfbleu.get_bleu_parallel()
    else:
        result_selfbleu = -1

    return round(result_bleu, 4), round(result_posbleu, 4), round(result_selfbleu, 4)

if __name__ == '__main__':
    opt = parser.parse_args()
    gram=opt.gram
    test_text = opt.test
    real_text = opt.real
    output_path = opt.output
    metric_list = opt.metric

    result_bleu, result_posbleu, result_selfbleu = compute_bleu(gram, test_text, real_text, metric_list)

    print('BLEU ' , gram , ': ', str(result_bleu))
    print('POSBLEU ' , gram , ': ', str(result_posbleu))
    print('SELFBLEU ' , gram , ': ', str(result_selfbleu))
    output_str = str(gram) + ';' + str(result_bleu) + ';' + str(result_posbleu) + ';' + str(result_selfbleu) + ';\n'
    with open(output_path, 'a') as f:
        f.write(output_str)