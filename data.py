import os
import torch
import numpy as np
import re

from collections import Counter

class Dictionary(object):
    def __init__(self):
        self.stoi = {}
        self.itos = []
        self.counter = Counter()
        self.total = 0

    def add_word(self, word):
        if word not in self.stoi:
            self.stoi[word] = len(self.itos)
            self.itos.append(word)
        token_id = self.stoi[word]
        self.counter[token_id] += 1
        self.total += 1
        return self.stoi[word]

    def get_id(self, word):
        return self.stoi.get(word)

    def get_word(self, id):
        return self.itos[id]

    def __len__(self):
        return len(self.stoi)


class Corpus(object):
    def __init__(self, input_path, vocab_path=None):
        self.dictionary = Dictionary()
        self.tokens = self.tokenize(input_path)

    def tokenize(self, input_path):
        """Tokenizes a text file."""
        # Add words to the dictionary
        self.dictionary.add_word('<sos>')

        with open(input_path, 'r') as f:
            tokens = 0
            for line in f:
                words = line.split()
                tokens += len(words) + 1
                for word in words:
                    self.dictionary.add_word(word)

        self.dictionary.add_word('<pad>')

        # Tokenize file content
        with open(input_path, 'r') as f:
            ids = torch.LongTensor(tokens)
            token = 0
            for line in f:
                words = line.split() + ['<pad>']
                for word in words:
                    ids[token] = self.dictionary.stoi.get(word)
                    token += 1

        return ids