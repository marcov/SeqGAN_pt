# -*- coding:utf-8 -*-

import os
import random
import math

import tqdm

import numpy as np
import torch

from torch.nn.utils.rnn import pad_sequence

class GenDataIter(object):

    def __init__(self, data_file, batch_size, sequence_len, pad_token=2):
        super(GenDataIter, self).__init__()
        self.batch_size = batch_size
        self.sequence_len = sequence_len
        self.pad_token = pad_token

        self.data_lis = self.read_file(data_file)
        self.data_num = len(self.data_lis)
        self.indices = range(self.data_num)
        self.num_batches = int(math.ceil(float(self.data_num)/self.batch_size))
        self.idx = 0

        self.reset()

    def __len__(self):
        return self.num_batches

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def reset(self):
        self.idx = 0
        random.shuffle(self.data_lis)

    def next(self):
        if self.idx >= self.data_num or self.data_num - self.idx < self.batch_size:
            raise StopIteration
        index = self.indices[self.idx:self.idx+self.batch_size]
        length = [min(len(self.data_lis[i]) + 1, self.sequence_len) for i in index]
        data = [self.data_lis[i] for i in index]
        data_tensor = torch.tensor(np.asarray(data, dtype='int64'), dtype=torch.long)
        data_tensor = torch.narrow(data_tensor, 1, 0, min(data_tensor.size(1), self.sequence_len - 1))
        data = torch.cat([torch.zeros(self.batch_size, 1, dtype=torch.long), data_tensor], dim=1)
        target = torch.cat([data_tensor, torch.full((self.batch_size, 1), self.pad_token, dtype=torch.long)], dim=1)
        self.idx += self.batch_size
        return data, target, length

    def read_file(self, data_file):
        with open(data_file, 'r') as f:
            lines = f.readlines()
        lis = []
        for line in lines:
            l = line.strip().split(' ')
            l = [int(s) for s in l]
            lis.append(l)
        return lis

class DisDataIter(object):

    def __init__(self, real_data_file, fake_data_file, batch_size, sequence_len, pad_token=0):
        super(DisDataIter, self).__init__()
        self.batch_size = batch_size
        self.sequence_len = sequence_len
        self.pad_token = pad_token

        self.real_data_lis = self.read_file(real_data_file)
        self.fake_data_lis = self.read_file(fake_data_file)

        self.data = self.real_data_lis + self.fake_data_lis
        self.labels = [1 for _ in range(len(self.real_data_lis))] +\
                        [0 for _ in range(len(self.fake_data_lis))]
        self.pairs = list(zip(self.data, self.labels))

        self.data_num = len(self.pairs)
        self.indices = range(self.data_num)
        self.num_batches = int(math.ceil(float(self.data_num)/self.batch_size))
        self.idx = 0

        self.reset()

    def __len__(self):
        return self.num_batches

    def __iter__(self):
        return self

    def __next__(self):
        return self.next()

    def reset(self):
        self.idx = 0
        random.shuffle(self.pairs)

    def next(self):
        if self.idx >= self.data_num or self.data_num - self.idx < self.batch_size:
            raise StopIteration
        index = self.indices[self.idx:self.idx+self.batch_size]
        length = [min(len(self.data[i])+1, self.sequence_len) for i in index]
        data, label = zip(*[self.pairs[i] for i in index])
        data = torch.tensor(np.asarray(data, dtype='int64'), dtype=torch.long)
        label = torch.tensor(np.asarray(label, dtype='int64'), dtype=torch.long)
        # reference = self.get_reference()
        self.idx += self.batch_size
        return data, label # , reference

    def get_reference(self):
        ref_samples = []
        for _ in range(4):
            ref_samples.append(self.real_data_lis[random.randint(0, len(self.real_data_lis) - 1)])
        return torch.tensor(np.array(ref_samples), dtype=torch.long)

    def read_file(self, data_file):
        with open(data_file, 'r') as f:
            lines = f.readlines()
        lis = []
        for line in lines:
            l = line.strip().split(' ')
            l = [int(s) for s in l]
            lis.append(l)
        return lis
