import argparse

def init_parser():
	parser = argparse.ArgumentParser(description='Training Parameter')

	# Model parameters.
	parser.add_argument('--save', type=str, default=None,
		help='save checkpoint in path')
	parser.add_argument('--resume', type=str, default=None,
		help='resume checkpoint in path')
	parser.add_argument('--only_gen', action='store_true',
		help='restore only generator')
	parser.add_argument('--gdrive', type=str, default=None,
		help='upload checkpoint to Google Drive id path')
	parser.add_argument('--checkpoint_interval', type=int, default=10,
		help='create a checkpoint every n epochs')

	# GAN parameters
	parser.add_argument('--gan_epochs', type=int, default=50,
		help='adversarial training epochs')
	parser.add_argument('--batch_size', type=int, default=4,
		help='adversarial training batch size')
	parser.add_argument('--generated_num', type=int, default=500,
		help='n of sequences used as fake data')
	parser.add_argument('--sequence_len', type=int, default=20,
		help='length of sequences')
	parser.add_argument('--ppl_threshold', type=float, default=2.2)

	# Data parameters
	parser.add_argument('--target_data', type=str, default="datasets/image_coco/sample.txt",
		help='path to real data')
	parser.add_argument('--train_data', type=str, default="train.data")
	parser.add_argument('--gene_data', type=str, default="gene.data")
	parser.add_argument('--eval_data', type=str, default="eval.data")
	parser.add_argument('--text_data', type=str, default="text.data")

	# Generator training parameters
	parser.add_argument('--g_pre_epochs', type=int, default=100,
		help='generator pre-training epochs')
	parser.add_argument('--g_steps', type=int, default=1,
		help='training steps per adversarial epoch')
	parser.add_argument('--g_epochs', type=int, default=1,
		help='training epochs per step')
	parser.add_argument('--g_early_stop', type=int, default=0)
	parser.add_argument('--g_post_epochs', type=int, default=0)
	# Generator parameters
	parser.add_argument('--g_pre_embed', type=int, default=0)
	parser.add_argument('--g_embed_size', type=int, default=32)
	parser.add_argument('--g_hidden_size', type=int, default=32)
	parser.add_argument('--g_num_layers', type=int, default=1)
	parser.add_argument('--g_embed_dropout', type=float, default=0)
	parser.add_argument('--g_layer_dropout', type=float, default=0)
	parser.add_argument('--g_out_dropout', type=float, default=0)
	parser.add_argument('--g_temperature', type=float, default=1.0)
	# Generator optimizer parameters
	parser.add_argument('--g_opt_wdecay', type=float, default=0)
	parser.add_argument('--g_opt_lr', type=float, default=1e-2)
	parser.add_argument('--gan_lr', type=float, default=1e-2)

	# Discriminator training parameters
	parser.add_argument('--d_pre_steps', type=int, default=50)
	parser.add_argument('--d_pre_epochs', type=int, default=3)
	parser.add_argument('--d_steps', type=int, default=5)
	parser.add_argument('--d_epochs', type=int, default=3)
	parser.add_argument('--d_early_stop', type=int, default=0,
		help='for each epoch train n batches')
	parser.add_argument('--d_loss_threshold', type=float, default=0.)
	parser.add_argument('--d_type', type=str, default="cnn",
		help='discriminator model [cnn, self, lstm, indrnn]')

	# Discriminator parameters
	parser.add_argument('--d_batch_size', type=int, default=64)
	parser.add_argument('--d_embed_size', type=int, default=64)
	parser.add_argument('--d_dropout', type=float, default=0.75)
	parser.add_argument('--d_num_class', type=int, default=2)
	parser.add_argument('--d_filter_sizes', type=str, default="1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20",
		help='filter size for cnn discriminator')
	parser.add_argument('--d_num_filters', type=str, default="100, 200, 200, 200, 200, 100, 100, 100, 100, 100, 160, 160",
		help='num filters for cnn discriminator')

	# Discriminator optimizer parameters
	parser.add_argument('--d_opt_lr', type=float, default=1e-4)

	return parser
