import os

class Logger():

    def __init__(self, conf, save=None):
        if not os.path.exists('./logs'):
            os.mkdir('./logs')
        logs_path = './logs/log'
        if save is not None:
            logs_path = './logs/log_' + save
        if not os.path.exists(logs_path):
            os.mkdir(logs_path)
        self.conf = open('%s/conf_log.txt' % logs_path, 'a')
        self.gen = open('%s/gen_log.txt' % logs_path, 'a')
        self.ppl = open('%s/ppl_log.txt' % logs_path, 'a')
        self.dis = open('%s/dis_log.txt' % logs_path, 'a')
        self.gan = open('%s/gan_log.txt' % logs_path, 'a')
        self.disg = open('%s/disg_log.txt' % logs_path, 'a')

        self.conf.write(conf)
        self.gen.write('epoch;loss;\n')
        self.ppl.write('epoch;loss;\n')
        self.dis.write('step;epoch;loss;\n')
        self.gan.write('iteration;step;loss;\n')
        self.disg.write('iteration;step;epoch;loss;\n')

    def write_gen(self, message):
        self.gen.write(message + ';\n')

    def write_ppl(self, message):
        self.ppl.write(message + ';\n')

    def write_dis(self, message):
        self.dis.write(message + ';\n')

    def write_gan(self, message):
        self.gan.write(message + ';\n')

    def write_dis_gan(self, message):
        self.disg.write(message + ';\n')

    def flush(self):
        self.conf.flush()
        os.fsync(self.conf.fileno())
        self.gen.flush()
        os.fsync(self.gen.fileno())
        self.ppl.flush()
        os.fsync(self.ppl.fileno())
        self.dis.flush()
        os.fsync(self.dis.fileno())
        self.gan.flush()
        os.fsync(self.gan.fileno())
        self.disg.flush()
        os.fsync(self.disg.fileno())

    def close(self):
        self.conf.close()
        self.gen.close()
        self.ppl.close()
        self.dis.close()
        self.gan.close()
        self.disg.close()
