import torch
import torch.nn as nn
import torch.nn.functional as F

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class Rewarded_NLLLoss(nn.Module):
    """Reward-Refined NLLLoss Function for adversial training of Generator"""
    def __init__(self):
        super(Rewarded_NLLLoss, self).__init__()

    def forward(self, logits, target, reward):
        """
        Args:
            logits: (N, C), torch
            target : (N, ), torch
            reward : (N, ), torch
        """
        loss = F.cross_entropy(logits, target, reduce=None)
        loss = torch.mean(loss * reward)
        return loss
